[index](README.md)
# SSH

To connect to a remote host, TU Delft uses a *proxy server*, know as **bastion**. Thefore, to reach a remote host, a user has to connect first to the **bastion** and from there to the remote host, e.g., *my-vm.tudelft.nl*. However, using SSH *proxy* or *jumphost*, a user can connect directly to the remote host.

## Using an SSH Proxy for a Host (with a linux SSH client)

1. On your local machine, edit the `~/.ssh/config` file and add the following confuration:

```bash
Host <host-nickname>
    Hostname <target-host>
    User <target-username>
    ProxyCommand ssh -W %h:%p <NetID>@linux-bastion-ex.tudelft.nl
```
Replace:
- `<host-nickname>`: a name for you choice for the target host, e.g. *my-server*
- `<target-host>`: the actual name of the target host (FQDM), e.g. `*server*.tudelft.nl`
- `<target-username>`: your username for the target host
- `<NetID>`: your TU Delft NetID username

2. Create an SSH key-pair on the local machine:

```bash
$ ssh-keygen -f ~/.ssh/<my-keyname> -t rsa -b 4096
```
You will be prompted to create a *passphrase* (required to secure the key against loss or theft). The passphrase will be asked every time you connect to the target host.

A private and public key-pair will be added to `~/.ssh`. The public key is in the file `<my-keyname>.pub`

>IMPORTANT: if your private key is stored in other location than `~/.ssh`, you must include the following line as part of the *Host* configuration (`~/.ssh/config`)
```bash
    IdentityFile /<path-to>/<my-keyname>
```

3. Add your public key into the `~/.ssh/authorized_keys` file in the target host. Use your *NetID password* for connecting to the bastion, and your *target-password* for connection to the <target-host>.

```bash
$ ssh-copy-id -i /<path-to>/<my-keyname.pub> <host-nickname>
```

4. Connect to the target host using *SSH Proxy*. Use your *NetID password* for connecting to the bastion, and your *passphrase* for connection to the <target-host>.

```bash
$ ssh <host-nickname>
```

If you encounter problems with the connection. Use the debug mode `ssh -vvv <host-nickname>` to find out what might have gone wrong.


## Tunneling with WinSCP
If using winscp, you will find detailed information here: https://winscp.net/eng/docs/ui_login_tunnel


# Moving Data Around

## Copy Data from Client to Host

```bash
$ scp <my-local-file> <host-nickname>:/<remote-directory>/
```
## Copy Data from Host to Client

```bash
$ scp <host-nickname>:/<path-to>/<my-remote-file> /<my-local-directory>/
```

## Copy data from one Host to another Host
Sometimes it's necessary to copy large amounts of data from one host to another host (for example when migrating data from one server to another), but you can't make a direct connection between those hosts (because of a firewall). It becomes even more of a challenge when your account for logging in doesn't have access to the data. But it's still possible to do this in one step.

We do this by performing the copy from a host that can connect to both hosts (like the *bastion*). On that host run the following command:
```
$ ssh <source-host> 'tar --create --file - --directory /<path-to-data>/ .' | ssh <destination-host> 'tar --extract --file - --directory /<path-to-destination>/ --verbose'
```
This command is a so-called pipeline: the `|` symbol creates a pipe to transfer the data from the *source* (`ssh <source.tudelft.nl> ...`) to the *destination* (`ssh <destination.tudelft.nl> ...`).

The *source* of the pipeline needs to connect to the source host, read the data and send it to the pipe. As before, `ssh` is used to connect to the source host. You need to specify the hostname (`<source>.tudelft.nl`) or a nickname, and also your username on the source host if needed (`<source-username>@<source-host>`). `tar` is used to read the data from the source directory (`/<path-to-data>/`). If you need admin access to read that data, run the `tar` command using `sudo` (`sudo tar --create ...`).

The *destination* of the pipeline needs to connect to the destination host and write the data to the destination location. `ssh` is used to connect to the destination host (specify the hostname (`<destination>.tudelft.nl`) or a nickname, and your username on the destination host if needed. `tar` is used to write the data to the destination location (`/<path-to-destination>/`). If you need admin access to write to that location, run the `tar` command using `sudo` (`sudo tar --extract ...`).

Example (using `sudo`):
```
$ ssh myuser@myserver.tudelft.nl 'sudo tar --create --file - --directory /data/ .' | ssh newuser@newserver.tudelft.nl 'sudo tar --extract --file - --directory /data/ --verbose'
```
